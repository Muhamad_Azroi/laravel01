<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function master(){
        return view('adminlte/master');
    }
    public function table(){
        return view('adminlte/table');
    }
    public function welcomee(Request $request){
        // var_dump($request);
        // dd($request["nama"]);
        return view('welcomee');
    }
    public function welcomee_post(Request $request){
        // dd($request->all());
        $first = $request ["First_Name"];
        $last = $request ["Last_Name"];
        $name = $first." ". $last;
        return view('welcomee',['nama' => $name]);
    }
}
