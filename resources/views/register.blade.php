<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
      @csrf
      <label> First Name: </label><br /><br />
      <input type="text" name="First_Name" /><br /><br />
      <label> Last Name: </label><br /><br />
      <input type="text" name="Last_Name" /><br /><br />
      <label> Gender </label><br /><br />
      <input type="radio" name="Gender" /> Male <br />
      <input type="radio" name="Gender" /> Female <br />
      <input type="radio" name="Gender" /> Other <br /><br />
      <label for="">Nationaly:</label><br /><br />
      <select name="Nationaly">
        <option value="">Indonesia</option>
        <option value="">Inggris</option>
        <option value="">Malaysia</option>
      </select>
      <br /><br />
      <label>Languange Spoken:</label><br /><br />
      <input type="checkbox" /> Bahasa Indonesia <br />
      <input type="checkbox" /> English <br />
      <input type="checkbox" /> Other <br />
      <br />
      <label>Bio:</label><br /><br />
      <textarea name="text" id="" cols="30" rows="10"></textarea><br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
