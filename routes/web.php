<?php

// use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome/{nama}', function ($nama) {
//     return view("welcomee", ["nama" => $nama]);
// });
// Route::get('/register', function () {
//     return view('register');
// });
// Route::get('/', function () {
//     return view('home');
// });

Route::get('/register', 'AuthController@register');
Route::get('/welcome', 'AuthController@welcomee');
Route::get('/', 'HomeController@home');
Route::post('/welcome', 'AuthController@welcomee_post');
Route::get('/master', 'AuthController@master');


Route::get('/table', function () {
    return view('table');
});
Route::get('/data-tables', function () {
    return view('data-tables');
});